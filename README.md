
# KeisOK Web Application
## Development
###Environment Setup

1. Download and Install [Node](https://nodejs.org/en/)  You will want version 6.11 or greater.
1. From a command prompt install [Angular CLI](https://github.com/angular/angular-cli)  using the command `npm install -g @angular/cli`
1. Install Typescript 2.0 `npm i -g typescript`
1. Install TSLint `npm install -g tslint`
1. Install Protractor for e2e testing `npm install -g protractor`
1. Install Node packages `npm i`
1. Update Webdriver `webdriver-manager update` and `./node_modules/.bin/webdriver-manager update`
1. Run local build `ng serve -o`

### Development server
Run `ng serve -o` for a dev server. This will open a browser and navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
### Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.
### Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
### Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
### Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.
### Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
##Deployment
