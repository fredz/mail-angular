import {NgModule, Type} from "@angular/core";
import {BrowserModule, Title} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CovalentHttpModule} from "@covalent/http";
import {CovalentHighlightModule} from "@covalent/highlight";
import {CovalentMarkdownModule} from "@covalent/markdown";
import {
  MdListModule,
  MdCardModule,
} from "@angular/material";
import {AppComponent} from "./app.component";
import {RequestInterceptor} from "../config/interceptors/request.interceptor";
import {MOCK_API, REAL_API} from "../config/api.config";
import {routedComponents, AppRoutingModule} from "./app-routing.module";
import {SharedModule} from "./modules/shared/shared.module";
import { MESSAGE_SEARCH_PROVIDER, MESSAGE_SEARCH_API } from './modules/search';
import {CovalentChipsModule} from "@covalent/core";
import {environment} from "../environments/environment.prod";
import { DonutPercentageDirective } from './donut-percentage.directive';

import { AuthenticationService } from './authentication.service';

// services
import { PercentBarService } from './percent-bar.service';

const httpInterceptorProviders: Type<any>[] = [
  RequestInterceptor,
];

export function getAPI(): string {
  if (environment.production)
    return REAL_API;
  else
    return MOCK_API;
}

@NgModule({
  declarations: [
    AppComponent,
    routedComponents,
    DonutPercentageDirective,
  ], // directives, components, and pipes owned by this NgModule
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    CovalentHttpModule.forRoot({
      interceptors: [{
        interceptor: RequestInterceptor, paths: ['**'],
      }],
    }),
    CovalentHighlightModule,
    CovalentMarkdownModule,
    CovalentChipsModule,
    // material modules
   MdCardModule,
    MdListModule,

  ], // modules needed to run this module
  providers: [
    httpInterceptorProviders,
    PercentBarService,
    Title,
    {
      provide: MESSAGE_SEARCH_API, useFactory: getAPI,
    },
    MESSAGE_SEARCH_PROVIDER,
    AuthenticationService,

  ], // additional providers needed for this module
  entryComponents: [ ],
  bootstrap: [ AppComponent ],
})
export class AppModule {}
