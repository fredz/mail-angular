import {Component, AfterViewInit, OnInit, ChangeDetectorRef} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {TdLoadingService, TdMediaService, IPageChangeEvent} from "@covalent/core";
import {MessageSearchService, IMessageResult, IDoc} from "./services/message-search.service";
import "rxjs/add/operator/toPromise";
import {Router} from "@angular/router";


export interface ISearchFilter {from:string, to:string, subject:string, content:string,page:number, pageSize:number , from_date:string, to_date:string, search_type:string };
@Component({
  selector: 'keisok-messages',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.scss'],
})
export class SearchComponent implements AfterViewInit, OnInit {

  isAnomaly: boolean = false

  messages: IMessageResult;
  docs: IDoc [];
  filteredSearchResults: IDoc[];
  filter: ISearchFilter;
  selected_message: IDoc;
  constructor(private _titleService: Title,
              private _loadingService: TdLoadingService,
              private _messageSearchService: MessageSearchService,
              private _changeDetectorRef: ChangeDetectorRef,
              public media: TdMediaService,
              private _router: Router
              ) {
  }

  ngOnInit(): void {
    this._titleService.setTitle('KeisOK Search');
    this.filter= {from:"", to:"", subject:"", content:"",page:1, pageSize:100, from_date:"", to_date:"", search_type:""};
    this.selected_message = {uuid_s: "", from_name_s: "",from_address_s:"",to_address_ss: [],subject_t: "",text_t: "", document_type_s: "",    message_date_dt: "", rank_i: 0, tags: [], selected:'', confidence_level:0};
    var empty = {numFound:0,docs:[]};
    this.messages ={response:empty};
  }

  ngAfterViewInit(): void {
    // broadcast to all listener observables when loading the page
    this.media.broadcast();
    // force a new change detection cycle since change detections
    // have finished when `ngAfterViewInit` is executed
    this._changeDetectorRef.detectChanges();
  }

  filterContent(searchString: string = ''): void {

    if (this.docs !== undefined) {
      this.filteredSearchResults = this.docs.filter(( doc: IDoc) => {
        return doc.text_t[0].toLowerCase().indexOf(searchString.toLowerCase()) > -1;
      });
    }
  }

  change(event: IPageChangeEvent): void {
    this.filter.page=event.page;
    this.filter.pageSize= event.pageSize;
    this.load();
  }
  onSubmit(searchType:string):void {    
    if (searchType == 'AnomalyDetection') {
      this.isAnomaly = true
    } else {
      this.isAnomaly = false
    }
    this.filter.search_type = searchType;
    this.load();
    this.selected_message = {
      uuid_s: "", 
      from_name_s: "",
      from_address_s:"",
      to_address_ss: [],
      subject_t: "",
      text_t: "", 
      document_type_s: "",    
      message_date_dt: "", 
      rank_i: 0, tags: [],
      selected:'',
      confidence_level: 0
    };
  }

  logout(): void {
    this._router.navigate(['']);
  }


  async load(): Promise<void> {
    try {
      console.log(this.filter)
      let filter = this.filter;
      let params: {[k: string]: any} = {}

      if (filter.from.length > 0) {
        params.from = filter.from
      }

      if (filter.to.length > 0) {
        params.to = filter.to
      }

      if (filter.subject.length > 0) {
        params.subject = filter.subject
      }

      if (filter.content.length > 0) {
        params.content = filter.content
      }

      if (filter.page) {
        params.page = filter.page
      }

      if (filter.pageSize) {
        params.pageSize = filter.pageSize
      }

      if (filter.from_date.length > 0) {
        params.from_date = filter.from_date
      }

      if (filter.to_date.length > 0) {
        params.to_date = filter.to_date
      }

      if (filter.search_type.length > 0) {
        params.search_type = filter.search_type
      }

      this._loadingService.register('messages.list');
      this.messages = await this._messageSearchService.query(params).toPromise();
    } catch (error) {
      this.messages = await this._messageSearchService.staticQuery().toPromise();
    } finally {
      console.log(this.messages)
      this.docs =  Object.assign([], this.messages.response.docs);
      this.filteredSearchResults = Object.assign([], this.docs);
      if(this.docs.length >0) {
        this.loadMessage(this.docs[0].uuid_s);
      }
      this._loadingService.resolve('messages.list');
    }
  }

   async loadMessage(documentId:string):Promise<void> {
     try {
       this._loadingService.register('message.load');
       let item: any;
       this.docs.forEach((s : IDoc) => {
         if (s.uuid_s === documentId) {
           item = s;
         }
       });
       this.selected_message = item;
      // this.selected_message = await this._messageSearchService.get(documentId).toPromise();
     } catch (error) {
       this.selected_message = await this._messageSearchService.staticGet(documentId).toPromise();
     } finally {
       this._loadingService.resolve('message.load');
     }
   }
}
