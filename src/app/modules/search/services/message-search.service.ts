import {Provider, SkipSelf, Optional, InjectionToken, Host } from "@angular/core";
import {Response, Headers} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {HttpInterceptorService, RESTService} from "@covalent/http";
import { AuthenticationService } from '../../../authentication.service';
// todo handle name/address values from server at some point
export interface IEmail {
  to_name:string;
  to_address: string;
}

export interface queryList {
  uuid_s: string;
  from: string;
  ro: string;
  subject: string;
  content: string;
  startDate: string;
  endDate: string;
  totalAlerts: number;
  totalWarnings: number;
  suspiciousActConf: number;
}

export interface IDoc {
  uuid_s: string;
  from_name_s: string;
  from_address_s:string;
  to_address_ss: string [];
  subject_t: string;
  text_t: string;
  document_type_s: string;
  message_date_dt: string;
  rank_i: number;
  tags: string [];
  selected:string;
  confidence_level: number;
}

export interface IMessageResult {
  response: {
    numFound: number;
    docs: IDoc[]
  }

}

export interface queryListResult {
  response: {
    numFound: number;
    docs: queryList[]
  }

}

export class MessageSearchService extends RESTService<IMessageResult> {

  constructor(private _http: HttpInterceptorService, api: string) {

    super(_http, {
      baseUrl: api,
      path: '/messages',
      dynamicHeaders: () => {
        return new Headers({ 'Authorization': 'Bearer ' + this.getTokenAccess() });
      },
    });

  }

  getTokenAccess(): string {

    const currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    const token: any = currentUser && currentUser.token;

    return token;
  }

  staticQuery(): Observable<IMessageResult> {
    return this._http.get('data/messages.json')
      .map((res: Response) => {
        return res.json();
      });
  }
  staticGet(document_id: string):  any {
    return this._http.get('data/messages.json')
      .map((res: Response) => {
        let docs = res.json().response.docs
        let item: any;
        docs.forEach((s  : IDoc) => {
          if (s.uuid_s === document_id) {
            item = s;
          }
        });
        return item;

      });
  }
}



export const MESSAGE_SEARCH_API: InjectionToken<string> = new InjectionToken<string>('MESSAGE_SEARCH_API');

export function MESSAGE_SEARCH_PROVIDER_FACTORY(
  parent: MessageSearchService, interceptorHttp: HttpInterceptorService, api: string):
   MessageSearchService {
  return parent || new MessageSearchService(interceptorHttp, api);
}

export const MESSAGE_SEARCH_PROVIDER: Provider = {
  // If there is already a service available, use that. Otherwise, provide a new one.
  provide: MessageSearchService,
   deps: [[new Optional(), new SkipSelf(), MessageSearchService], HttpInterceptorService, MESSAGE_SEARCH_API],
  useFactory: MESSAGE_SEARCH_PROVIDER_FACTORY,
};

