import { Injectable } from '@angular/core';
import {Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {HttpInterceptorService, RESTService} from "@covalent/http";


export interface queryList {
  from: string;
  ro: string;
  subject:string;
  content: string;
  startDate: string;
  endDate: string;
  totalAlerts: number;
  totalWarnings: number;
  suspiciousActConf: number;
}

export interface queryListResult {
  response: {
    numFound: number;
    docs: queryList[]
  }

}




@Injectable()
export class DashboardTrackerInformationService extends RESTService<queryListResult> {

    constructor(private _http: HttpInterceptorService, api: string) {
    super(_http, {
      baseUrl: api,
      path: '/query-watch'
    });
  }

  staticQuery(): Observable<queryListResult> {
    return this._http.get('data/query-watch.json')
      .map((res: Response) => {
        return res.json();
      });
  }
  staticGet(document_id: string):  any {
    return this._http.get('data/query-watch.json')
      .map((res: Response) => {
        let docs = res.json().response.docs
        let item: any;
        docs.forEach((s  : queryList) => {
          // if (s.uuid_s === document_id) {
          //   item = s;
          // }
        });
        return item;

      });
  }

}
