import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {
  MdSnackBarModule,
  MdIconModule,
  MdListModule,
  MdTooltipModule,
  MdCardModule,
  MdButtonModule,
  MdToolbarModule,
  MdInputModule,
  MdSlideToggleModule,
  MdMenuModule,
  MdDatepickerModule,
  MdNativeDateModule,
} from "@angular/material";
import {
  CovalentLoadingModule,
  CovalentDialogsModule,
  CovalentMediaModule,
  CovalentLayoutModule,
  CovalentSearchModule,
  CovalentCommonModule,
  CovalentPagingModule,
  CovalentChipsModule
} from "@covalent/core";
import {SearchComponent} from "./search.component";
import {DetailViewComponent} from "./detail/detail.component";
import {userRoutes} from "./search.routes";
import {
  MessageSearchService,
  IMessageResult,
  MESSAGE_SEARCH_PROVIDER,
  MESSAGE_SEARCH_API,
} from "./services/message-search.service";


import {
  DashboardTrackerInformationService,
  // queryListResult
  
} from "./services/dashboard-tracker-information.service";
import { ProgressBarComponent } from './progress-bar/progress-bar.component';


export { SearchComponent, DetailViewComponent, MessageSearchService,  IMessageResult, MESSAGE_SEARCH_PROVIDER, MESSAGE_SEARCH_API};

@NgModule({
  declarations: [
    SearchComponent,
    DetailViewComponent,
    ProgressBarComponent,
  ], // directives, components, and pipes owned by this NgModule
  imports: [
    // angular modules
    CommonModule,
    FormsModule,
    RouterModule,
    // material modules
    MdSnackBarModule,
    MdIconModule,
    MdListModule,
    MdTooltipModule,
    MdCardModule,
    MdButtonModule,
    MdToolbarModule,
    MdInputModule,
    MdSlideToggleModule,
    MdMenuModule,
    MdNativeDateModule,
    MdDatepickerModule,
    // covalent modules
    CovalentLoadingModule,
    CovalentDialogsModule,
    CovalentMediaModule,
    CovalentLayoutModule,
    CovalentSearchModule,
    CovalentCommonModule,
    CovalentPagingModule,
    CovalentChipsModule,

    // extra
    userRoutes,
  ], // modules needed to run this module
  providers: [
    { provide: MESSAGE_SEARCH_API, useValue: ''},
    MESSAGE_SEARCH_PROVIDER,
  ],
})
export class SearchModule {}
