import {ModuleWithProviders} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {SearchComponent} from "./search.component";
import {DetailViewComponent} from "./detail/detail.component";

const routes: Routes = [{
    path: 'search',
    children: [{
      path: '',
      component: SearchComponent,
      children:[{
        path: ':documentId/view',
        component: DetailViewComponent,
      }]

    },
      {
        path: ':documentId/viewnew',
        component: DetailViewComponent,
      }
   ],
}];

export const userRoutes: ModuleWithProviders = RouterModule.forChild(routes);
