import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { PercentBarService } from '../../../percent-bar.service'

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit, AfterViewInit {
	@Input() dataIndex: number;
	@Input() confidence: number;

  constructor(private percentageBar: PercentBarService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  	this.percentageBar.renderPercentBar(this.dataIndex, this.confidence)
  }



}
