import {Component, OnInit} from "@angular/core";
import {Location} from '@angular/common';
import {ActivatedRoute} from "@angular/router";
import {TdLoadingService} from "@covalent/core";
import {MessageSearchService,  IDoc} from "../services/message-search.service";
import "rxjs/add/operator/toPromise";

@Component({
  selector: 'keisok-detail-view',
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
})
export class DetailViewComponent implements OnInit {

  message: IDoc;
  uuid_s: string;


  constructor(private _location: Location,
              private _messageSearchService: MessageSearchService,
              private _route: ActivatedRoute,
              private _loadingService: TdLoadingService) {}

  ngOnInit(): void {

    this.message = {uuid_s: "", from_name_s: "",from_address_s:"",to_address_ss: [],subject_t: "",text_t: "", document_type_s: "",    message_date_dt: "", rank_i: 0, tags: [], selected:'', confidence_level:0};

    this._route.params.subscribe((params: {uuid_s: string}) => {
      this.uuid_s= params.uuid_s;
      if (this.uuid_s) {
        this.load();
      }
    });
  }
  navigateBack():void{
    this._location.back();
  }

  async load(): Promise<void> {
    try {
      this._loadingService.register('message.view');
      let message: IDoc = await this._messageSearchService.get(this.uuid_s).toPromise();
      this.message = message;
    } catch (error) {
      let message: IDoc = await this._messageSearchService.staticGet(this.uuid_s).toPromise();
      this.message = message;
    } finally {
      this._loadingService.resolve('message.view');
    }
  }

}
