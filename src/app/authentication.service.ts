import { Injectable, Inject } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { MESSAGE_SEARCH_API } from './modules/search';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';


@Injectable()
export class AuthenticationService {

  public token: string;
  public headers: any = new Headers({ 'Content-Type': 'application/json' });
  public options: any = new RequestOptions({ headers: this.headers });

  constructor(private http: Http, @Inject(MESSAGE_SEARCH_API) private _apiEndpoint: string ) {
    // set token if saved in local storage
    const currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;

  }

  prepareHeaders(): RequestOptions {
    // add authorization header with jwt token
    const headers: Headers = new Headers({ 'Authorization': 'Bearer ' + this.token });
    const options: RequestOptions = new RequestOptions({ headers: headers });

    return options;
  }

  login(username: string, password: string): Observable<boolean> {
    return this.http.post(this._apiEndpoint + '/login', JSON.stringify({ username: username, password: password }), this.options)
        .map((response: Response) => {
            // login successful if there's a jwt token in the response
            const accessToken: string = response.json() && response.json().access_token;
            if (accessToken && response.json().login ) {

                // set token property
                this.token = accessToken;

                // store username and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify({ username: username, token: accessToken }));

                // return true to indicate successful login
                return true;
            } else {
                // return false to indicate failed login
                return false;
            }
        });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = undefined;
    localStorage.removeItem('currentUser');
  }
}
