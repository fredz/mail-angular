import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {MainComponent} from "./main/main.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'keisok',
        component: MainComponent,
        children: [
            {
                component: DashboardComponent,
                path: ''
            },
            { path: '', loadChildren: './modules/search/search.module#SearchModule' },
        ],
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { useHash: true }),
    ],
    exports: [
        RouterModule,

    ]
})
export class AppRoutingModule { }
export const routedComponents: any[] = [
    MainComponent,
    LoginComponent,
    DashboardComponent,
];
