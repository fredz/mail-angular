import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {TdLoadingService} from "@covalent/core";
import { Http } from '@angular/http';
import {Headers} from '@angular/http';
import { AuthenticationService } from '../authentication.service';


@Component({
  selector: 'keisok-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {

  username: string;
  password: string;
  error: string;

  constructor(private _router: Router,
              private _loadingService: TdLoadingService,
              public http: Http,
              private _authenticationService: AuthenticationService) {
              }

  login(): void {

    this._loadingService.register();

    /* let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let body = JSON.stringify({ "username": this.username, "password": this.password });

    this.http.post(this.api + '/login', body, {headers})
      .subscribe(
        response => {
          // localStorage.setItem('id_token', response.json().id_token);
          this._router.navigate(['/keisok/search']);
          this._loadingService.resolve();
        },
        error => {
          // alert(error.text());
          console.log(error);
          this._router.navigate(['/']);
          console.log(error);
          this._loadingService.resolve();
        }
      );*/

    this._authenticationService.login(this.username, this.password)
    .subscribe((result: boolean) => {
        if (result === true) {

            // login successful
            this._router.navigate(['/keisok/search']);
            this._loadingService.resolve();
        } else {
            // login failed
            this.error = 'Username or password is incorrect';

            console.log(this.error);
            this._router.navigate(['/']);
            console.log(this.error);
            this._loadingService.resolve();

        }
    });

  }
}
