import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'keisok-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent {

  routes: Object[] = [{
      title: 'Dashboard',
      route: '/keisok',
      icon: 'dashboard',
    }, {
      title: 'Search',
      route: '/keisok/search',
      icon: 'search',
    },
  ];

  constructor(private _router: Router) {}

  logout(): void {
    this._router.navigate(['/logout']);
  }
}
