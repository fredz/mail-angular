import { Injectable } from '@angular/core';
// import * as d3 from "add-on_modules/d3.min"

import * as d3 from 'd3';

@Injectable()
export class PercentBarService {

  constructor() { }

  renderPercentBar(index, confidence): void {

    let svg: any = d3.select('.bar' + index).append('svg')
        .attr('height', 15)
        .attr('width', 50);

    let states: any = ['started', 'inProgress', 'completed'];
    let domain: any = [50, 100];
    // segmentWidth = 50,
    let currentState: any  = 'completed';

    // var colorScale = d3.scale.linear()
    let colorScale: any  = d3.scaleLinear()
      .domain(domain)
      .range(['yellow', '#29c638']);

    svg.append('rect')
      .attr('class', 'bg-rect')
      .attr('rx', 5)
      .attr('ry', 5)
      .attr('fill', 'lightgray')
      .attr('height', 15)
      .attr('width', function(){
        return 50;
        })
      .attr('x', 0);

    let progress: any = svg.append('rect')
      .attr('class', 'progress-rect')
      .attr('fill', function(){
              return colorScale(confidence);
      })
      .attr('height', 15)
      .attr('width', 0)
      .attr('rx', 5)
      .attr('ry', 5)
      .attr('x', 0);

    progress.transition()
      .duration(1000)
      .attr('width', function(){
        // var index = states.indexOf(currentState);
        return confidence * 0.5;
    });

    function moveProgressBar(state){
      progress.transition()
      .duration(1000)
      .attr('fill', function(){
        return colorScale(state);
      })
      .attr('width', function(){
        // var index = states.indexOf(state);
        return confidence * 1.5;
      });
    }
  }

}
