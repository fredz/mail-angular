import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {TdLoadingService} from "@covalent/core";
import {AlertsService} from "../../services";
import {queryListService} from "../../services";
import {IMessageResult, IDoc, queryList, queryListResult} from "../modules/search/services/message-search.service";
// import {queryList, queryListResult} from "../modules/search/services/dashboard-tracker-information.service";
// import { queryList, DashboardTrackerInformationService } from "../modules/search/services/dashboard-tracker-information.service";
// import { DonutPercentageDirective } from "donut-percentage.directive"
// import * as d3 from "add-on_modules/d3.min"
import * as d3 from 'd3';

@Component({
  selector: 'kiesOK-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  viewProviders: [ AlertsService, queryListService ],
})
export class DashboardComponent implements OnInit {


  alerts: IDoc[];
  message: IDoc;
  qMessage: queryList;
  qAlert: queryList[];



  constructor(private _titleService: Title,
              private _alertsService: AlertsService,
              private _queryList: queryListService,
              private _loadingService: TdLoadingService) {
  }

  ngOnInit(): void {


    this.message = {uuid_s: "", from_name_s: "",from_address_s:"",to_address_ss: [],subject_t: "",text_t: "", document_type_s: "",    message_date_dt: "", rank_i: 0, tags: [], selected:'', confidence_level:0};
    this._titleService.setTitle( 'KeisOK Dashboard' );
    this._loadingService.register('alerts.load');
    this._alertsService.query().subscribe((alerts: IMessageResult) => {
      this.alerts = alerts.response.docs;
      setTimeout(() => {
        this._loadingService.resolve('alerts.load');
      }, 750);
      this._loadingService.register('message.load');
      this._alertsService.get(this.alerts[0].uuid_s).subscribe((message: IDoc) => {
        this.message = message;
        setTimeout(() => {
          this._loadingService.resolve('message.load');
        }, 750);
      });
    });

    this.qMessage = {
      uuid_s: "",
      from: "",
      ro: "",
      subject:"",
      content:"",
      startDate:"",
      endDate:"",
      totalAlerts:0,
      totalWarnings:0,
      suspiciousActConf:0}
      this._loadingService.register('qAlert.load');
      this._queryList.query().subscribe((qAlert: queryListResult) => {
        console.log(qAlert)
        this.qAlert = qAlert.response['queries'];
        console.log(this.qAlert)
        setTimeout(() => {
        this._loadingService.resolve('qAlert.load');
      }, 750);
      this._loadingService.register('qMessage.load');
      this._queryList.get(this.qAlert[0].uuid_s).subscribe((qMessage: queryList) => {
        this.qMessage = qMessage;
        setTimeout(() => {
          this._loadingService.resolve('qMessage.load');
        }, 750);
      });
    });


   
function renderdonut() {
  const color = ['orange', 'gray']

var duration = 1500,
  transition = 200,
  percent = 65,
  width = document.getElementById('donut').clientWidth,
  height = 200;

var dataset = {
      lower: calcPercent(0),
      upper: calcPercent(percent)
  },
  radius = Math.min(width, height) / 2,
  pie = d3.layout.pie().sort(null),
  format = d3.format(".0%");

var arc = d3.svg.arc()
  .innerRadius(radius * .8)
  .outerRadius(radius);

var svg = d3.select("#donut").append("svg")
  .attr("width", width)
  .attr("height", height)
  .append("g")
  .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var path = svg.selectAll("path")
  .data(pie(dataset.lower))
  .enter().append("path")
  .attr("class", function (d, i) {
      return "color" + i
  })
  .attr('fill', function (d, i) {
    return color[i]
  })
  .attr("d", arc)
  .each(function (d) {
      this._current = d;
  });

var text = svg.append("text")
        .attr("text-anchor", "middle")
        .attr("dy", ".3em");

var progress = 0;

var timeout = setTimeout(function () {
    clearTimeout(timeout);
    path = path.data(pie(dataset.upper))
    path.transition().duration(duration).attrTween("d", function (a) {
        var i = d3.interpolate(this._current, a);
        var i2 = d3.interpolate(progress, percent)
        this._current = i(0);
        return function (t) {
            text.text(format(i2(t) / 100));
            return arc(i(t));
        };
    });
}, 200);

function calcPercent(percent) {
    return [percent, 100 - percent];
};

}



// ---------------------------------------


// ----------------------------------------------

function renderLineChart() {

var data = [
  {
    date: '31-Jul-17',
    rating: 43
  },
  {
    date: '30-Jul-17',
    rating: 47
  },
  {
    date: '29-Jul-17',
    rating: 41
  },
  {
    date: '28-Jul-17',
    rating: 43
  },
  {
    date: '27-Jul-17',
    rating: 47
  },
  {
    date: '26-Jul-17',
    rating: 41
  },
  {
    date: '25-Jul-17',
    rating: 41
  },
  {
    date: '24-Jul-17',
    rating: 49
  },
  {
    date: '23-Jul-17',
    rating: 47
  },
  {
    date: '22-Jul-17',
    rating: 47
  },
  {
    date: '21-Jul-17',
    rating: 48
  },
  {
    date: '20-Jul-17',
    rating: 48
  },
  {
    date: '19-Jul-17',
    rating: 48
  },
  {
    date: '19-Jul-17',
    rating: 49
  },
  {
    date: '17-Jul-17',
    rating: 46
  },
  {
    date: '16-Jul-17',
    rating: 47
  },
  {
    date: '15-Jul-17',
    rating: 46
  },
  {
    date: '14-Jul-17',
    rating: 46
  },
  {
    date: '13-Jul-17',
    rating: 47
  },
  {
    date: '12-Jul-17',
    rating: 47
  },
  {
    date: '11-Jul-17',
    rating: 43
  },
  {
    date: '10-Jul-17',
    rating: 40
  },
  {
    date: '9-Jul-17',
    rating: 39
  },
  {
    date: '8-Jul-17',
    rating: 40
  },
  {
    date: '7-Jul-17',
    rating: 41
  },
  {
    date: '6-Jul-17',
    rating: 46
  },
  {
    date: '5-Jul-17',
    rating: 47
  },
  {
    date: '4-Jul-17',
    rating: 47
  },
  {
    date: '3-Jul-17',
    rating: 48
  },
  {
    date: '2-Jul-17',
    rating: 44
  },
  {
    date: '1-Jul-17',
    rating: 43
  },
  {
    date: '31-Jun-17',
    rating: 43
  },
  {
    date: '30-Jun-17',
    rating: 47
  },
  {
    date: '29-Jun-17',
    rating: 41
  },
  {
    date: '28-Jun-17',
    rating: 43
  },
  {
    date: '27-Jun-17',
    rating: 47
  },
  {
    date: '26-Jun-17',
    rating: 41
  },
  {
    date: '25-Jun-17',
    rating: 41
  },
  {
    date: '24-Jun-17',
    rating: 39
  },
  {
    date: '23-Jun-17',
    rating: 37
  },
  {
    date: '22-Jun-17',
    rating: 37
  },
  {
    date: '21-Jun-17',
    rating: 38
  },
  {
    date: '20-Jun-17',
    rating: 38
  },
  {
    date: '19-Jun-17',
    rating: 38
  },
  {
    date: '19-Jun-17',
    rating: 39
  },
  {
    date: '17-Jun-17',
    rating: 36
  },
  {
    date: '16-Jun-17',
    rating: 37
  },
  {
    date: '15-Jun-17',
    rating: 36
  },
  {
    date: '14-Jun-17',
    rating: 36
  },
  {
    date: '13-Jun-17',
    rating: 37
  },
  {
    date: '12-Jun-17',
    rating: 37
  },
  {
    date: '11-Jun-17',
    rating: 43
  },
  {
    date: '10-Jun-17',
    rating: 40
  },
  {
    date: '9-Jun-17',
    rating: 39
  },
  {
    date: '8-Jun-17',
    rating: 40
  },
  {
    date: '7-Jun-17',
    rating: 41
  },
  {
    date: '6-Jun-17',
    rating: 36
  },
  {
    date: '5-Jun-17',
    rating: 37
  },
  {
    date: '4-Jun-17',
    rating: 37
  },
  {
    date: '3-Jun-17',
    rating: 38
  },
  {
    date: '2-Jun-17',
    rating: 34
  },
  {
    date: '1-Jun-17',
    rating: 38
  },
  {
    date: '31-May-17',
    rating: 33
  },
  {
    date: '30-May-17',
    rating: 37
  },
  {
    date: '29-May-17',
    rating: 31
  },
  {
    date: '28-May-17',
    rating: 33
  },
  {
    date: '27-May-17',
    rating: 37
  },
  {
    date: '26-May-17',
    rating: 41
  },
  {
    date: '25-May-17',
    rating: 41
  },
  {
    date: '24-May-17',
    rating: 39
  },
  {
    date: '23-May-17',
    rating: 37
  },
  {
    date: '22-May-17',
    rating: 37
  },
  {
    date: '21-May-17',
    rating: 38
  },
  {
    date: '20-May-17',
    rating: 38
  },
  {
    date: '19-May-17',
    rating: 38
  },
  {
    date: '19-May-17',
    rating: 39
  },
  {
    date: '17-May-17',
    rating: 36
  },
  {
    date: '16-May-17',
    rating: 37
  },
  {
    date: '15-May-17',
    rating: 36
  },
  {
    date: '14-May-17',
    rating: 36
  },
  {
    date: '13-May-17',
    rating: 37
  },
  {
    date: '12-May-17',
    rating: 37
  },
  {
    date: '11-May-17',
    rating: 33
  },
  {
    date: '10-May-17',
    rating: 30
  },
  {
    date: '9-May-17',
    rating: 29
  },
  {
    date: '8-May-17',
    rating: 30
  },
  {
    date: '7-May-17',
    rating: 30
  },
  {
    date: '6-May-17',
    rating: 26
  },
  {
    date: '5-May-17',
    rating: 27
  },
  {
    date: '4-May-17',
    rating: 27
  },
  {
    date: '3-May-17',
    rating: 28
  },
  {
    date: '2-May-17',
    rating: 28
  },
  {
    date: '1-May-17',
    rating: 30
  },
  {
    date: '30-Apr-17',
    rating: 23
  },
  {
    date: '29-Apr-17',
    rating: 24
  },
  {
    date: '28-Apr-17',
    rating: 23
  },
  {
    date: '27-Apr-17',
    rating: 27
  },
  {
    date: '26-Apr-17',
    rating: 24
  },
  {
    date: '25-Apr-17',
    rating: 24
  },
  {
    date: '24-Apr-17',
    rating: 29
  },
  {
    date: '23-Apr-17',
    rating: 27
  },
  {
    date: '22-Apr-17',
    rating: 27
  },
  {
    date: '21-Apr-17',
    rating: 28
  },
  {
    date: '20-Apr-17',
    rating: 28
  },
  {
    date: '19-Apr-17',
    rating: 28
  },
  {
    date: '19-Apr-17',
    rating: 29
  },
  {
    date: '17-Apr-17',
    rating: 26
  },
  {
    date: '16-Apr-17',
    rating: 27
  },
  {
    date: '15-Apr-17',
    rating: 26
  },
  {
    date: '14-Apr-17',
    rating: 26
  },
  {
    date: '13-Apr-17',
    rating: 27
  },
  {
    date: '12-Apr-17',
    rating: 27
  },
  {
    date: '11-Apr-17',
    rating: 33
  },
  {
    date: '10-Apr-17',
    rating: 30
  },
  {
    date: '9-Apr-17',
    rating: 29
  },
  {
    date: '8-Apr-17',
    rating: 28
  },
  {
    date: '7-Apr-17',
    rating: 31
  },
  {
    date: '6-Apr-17',
    rating: 26
  },
  {
    date: '5-Apr-17',
    rating: 27
  },
  {
    date: '4-Apr-17',
    rating: 27
  },
  {
    date: '3-Apr-17',
    rating: 28
  },
  {
    date: '2-Apr-17',
    rating: 42
  },
  {
    date: '1-Apr-17',
    rating: 23
  },
  {
    date: '31-Mar-17',
    rating: 23
  },
  {
    date: '30-Mar-17',
    rating: 23
  },
  {
    date: '29-Mar-17',
    rating: 24
  },
  {
    date: '28-Mar-17',
    rating: 23
  },
  {
    date: '27-Mar-17',
    rating: 27
  },
  {
    date: '26-Mar-17',
    rating: 21
  },
  {
    date: '25-Mar-17',
    rating: 21
  },
  {
    date: '24-Mar-17',
    rating: 29
  },
  {
    date: '23-Mar-17',
    rating: 27
  },
  {
    date: '22-Mar-17',
    rating: 27
  },
  {
    date: '21-Mar-17',
    rating: 28
  },
  {
    date: '20-Mar-17',
    rating: 28
  },
  {
    date: '19-Mar-17',
    rating: 28
  },
  {
    date: '19-Mar-17',
    rating: 29
  },
  {
    date: '17-Mar-17',
    rating: 26
  },
  {
    date: '16-Mar-17',
    rating: 27
  },
  {
    date: '15-Mar-17',
    rating: 26
  },
  {
    date: '14-Mar-17',
    rating: 26
  },
  {
    date: '13-Mar-17',
    rating: 27
  },
  {
    date: '12-Mar-17',
    rating: 27
  },
  {
    date: '11-Mar-17',
    rating: 23
  },
  {
    date: '10-Mar-17',
    rating: 20
  },
  {
    date: '9-Mar-17',
    rating: 19
  },
  {
    date: '8-Mar-17',
    rating: 14
  },
  {
    date: '7-Mar-17',
    rating: 14
  },
  {
    date: '6-Mar-17',
    rating: 16
  },
  {
    date: '5-Mar-17',
    rating: 17
  },
  {
    date: '4-Mar-17',
    rating: 17
  },
  {
    date: '3-Mar-17',
    rating: 14
  },
  {
    date: '2-Mar-17',
    rating: 14
  },
  {
    date: '1-Mar-17',
    rating: 13
  },
  {
    date: '28-Feb-17',
    rating: 13
  },
  {
    date: '27-Feb-17',
    rating: 13
  },
  {
    date: '26-Feb-17',
    rating: 12
  },
  {
    date: '25-Feb-17',
    rating: 13
  },
  {
    date: '24-Feb-17',
    rating: 14
  },
  {
    date: '23-Feb-17',
    rating: 13
  },
  {
    date: '22-Feb-17',
    rating: 13
  },
  {
    date: '21-Feb-17',
    rating: 12
  },
  {
    date: '20-Feb-17',
    rating: 11
  },
  {
    date: '19-Feb-17',
    rating: 12
  },
  {
    date: '19-Feb-17',
    rating: 11
  },
  {
    date: '17-Feb-17',
    rating: 11
  },
  {
    date: '16-Feb-17',
    rating: 12
  },
  {
    date: '15-Feb-17',
    rating: 12
  },
  {
    date: '14-Feb-17',
    rating: 12
  },
  {
    date: '13-Feb-17',
    rating: 12
  },
  {
    date: '12-Feb-17',
    rating: 12
  },
  {
    date: '11-Feb-17',
    rating: 13
  },
  {
    date: '10-Feb-17',
    rating: 10
  },
  {
    date: '9-Feb-17',
    rating: 9
  },
  {
    date: '8-Feb-17',
    rating: 10
  },
  {
    date: '7-Feb-17',
    rating: 11
  },
  {
    date: '6-Feb-17',
    rating: 6
  },
  {
    date: '5-Feb-17',
    rating: 7
  },
  {
    date: '4-Feb-17',
    rating: 7
  },
  {
    date: '3-Feb-17',
    rating: 5
  },
  {
    date: '2-Feb-17',
    rating: 4
  },
  {
    date: '1-Feb-17',
    rating:6
  },
    {
    date: '31-Jan-17',
    rating: 7
  },
  {
    date: '30-Jan-17',
    rating: 8
  },
  {
    date: '29-Jan-17',
    rating: 8
  },
  {
    date: '28-Jan-17',
    rating: 8
  },
  {
    date: '27-Jan-17',
    rating: 7
  },
  {
    date: '26-Jan-17',
    rating: 8
  },
  {
    date: '25-Jan-17',
    rating: 8
  },
  {
    date: '24-Jan-17',
    rating: 9
  },
  {
    date: '23-Jan-17',
    rating: 7
  },
  {
    date: '22-Jan-17',
    rating: 7
  },
  {
    date: '21-Jan-17',
    rating: 8
  },
  {
    date: '20-Jan-17',
    rating: 8
  },
  {
    date: '19-Jan-17',
    rating: 8
  },
  {
    date: '19-Jan-17',
    rating: 9
  },
  {
    date: '17-Jan-17',
    rating: 6
  },
  {
    date: '16-Jan-17',
    rating: 7
  },
  {
    date: '15-Jan-17',
    rating: 6
  },
  {
    date: '14-Jan-17',
    rating: 6
  },
  {
    date: '13-Jan-17',
    rating: 7
  },
  {
    date: '12-Jan-17',
    rating: 7
  },
  {
    date: '11-Jan-17',
    rating: 8
  },
  {
    date: '10-Jan-17',
    rating: 7
  },
  {
    date: '9-Jan-17',
    rating: 9
  },
  {
    date: '8-Jan-17',
    rating: 6
  },
  {
    date: '7-Jan-17',
    rating: 6
  },
  {
    date: '6-Jan-17',
    rating: 6
  },
  {
    date: '5-Jan-17',
    rating: 7
  },
  {
    date: '4-Jan-17',
    rating: 7
  },
  {
    date: '3-Jan-17',
    rating: 8
  },
  {
    date: '2-Jan-17',
    rating: 4
  },
  {
    date: '1-Jan-17',
    rating: 3
  }
]

  var margin = {top: 30, right: 20, bottom: 30, left: 50},
    width = 500 - margin.left - margin.right,
    height = 200 - margin.top - margin.bottom;

// Parse the date / time
var parseDate = d3.time.format("%d-%b-%y").parse;

// Set the ranges
var x = d3.time.scale().range([0, width]);
var y = d3.scale.linear().range([height, 0]);

// Define the axes
var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);

var yAxis = d3.svg.axis().scale(y)
    .orient("left").ticks(5);

// Define the line
var valueline = d3.svg.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.rating); });
    
// Adds the svg canvas
var svg = d3.select("#analysis")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform", 
              "translate(" + margin.left + "," + margin.top + ")");

// Get the data
// d3.csv("./data.csv", function(error, data) {
    data.forEach(function(d) {
      // console.log(d)
        d.date = parseDate(d.date);
        // d.rating = +d.rating;
    });

    // Scale the range of the data
    x.domain(d3.extent(data, function(d) { return d.date; }));
    y.domain([0, d3.max(data, function(d) { return d.rating; })]);

    // Add the valueline path.
    svg.append("path")
        .attr("class", "line")
        .attr('stroke', 'steelblue')
        .attr('fill', 'none')
        .attr('stroke-width', '2px')
        .attr("d", valueline(data));

    // Add the X Axis
    svg.append("g")
        .attr("class", "x axis")
        .attr('stroke-width', '1px')
        .attr('fill', 'none')
        .attr('stroke', 'grey')
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    // Add the Y Axis
    svg.append("g")
        .attr("class", "y axis")
        .attr('stroke-width', '1px')
        .attr('fill', 'none')
        .attr('stroke', 'grey')
        .call(yAxis);

// });
}

renderdonut();
renderLineChart();



// ---------------------------------------


  }
  loadMessage(documentId:string){
    this._loadingService.register('message.load');
    this._alertsService.get(documentId).subscribe((message: IDoc) => {
      this.message = message;
      setTimeout(() => {
        this._loadingService.resolve('message.load');
      }, 750);
    });

  }

  loadQuery(documentId:string){
    this._loadingService.register('qMessage.load');
    this._alertsService.get(documentId).subscribe((qMessage: queryList) => {
      this.qMessage = qMessage;
      setTimeout(() => {
        this._loadingService.resolve('qMessage.load');
      }, 750);
    });

  }

}
