import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {HttpInterceptorService} from "@covalent/http";
@Injectable()
export class queryListService  {

  constructor(private _http: HttpInterceptorService) {
  }


  query(): any {
   return this._http.get('data/query-watch.json')
   .map((res: Response) => {
     console.log(res)
     return res.json();
   });
  }

  get(id: string): any {
   return this._http.get('data/query-watch.json')
   .map((res: Response) => {
     let queries = res.json().response.queries;
     let item: any;
     queries.forEach((s: any) => {
       if (s.uuid_s === id) {
         item = s;
       }
     });
     return item;
   });
  }
}
