import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {HttpInterceptorService} from "@covalent/http";
@Injectable()
export class AlertsService  {

  constructor(private _http: HttpInterceptorService) {
  }


  query(): any {
   return this._http.get('data/alerts.json')
   .map((res: Response) => {
     return res.json();
   });
  }

  get(id: string): any {
   return this._http.get('data/alerts.json')
   .map((res: Response) => {
     let docs = res.json().response.docs;
     let item: any;
     docs.forEach((s: any) => {
       if (s.uuid_s === id) {
         item = s;
       }
     });
     return item;
   });
  }
}
